// Jasmine Testing
// "describe" and "it" are present to oragnize properly your test.
// We can see as "describe" is the group and "it" an action of the group.

// Describe is similar at a general function
// where is included two parameters
// the first is a simple title of the "it" group.
// the second is function

describe("Testing Two Numbers", function() {
	var a = 4;
	var b = 3;
	var c = null;
	// "it" is the same of describe
  it("Add Numbers", function() {
		// toEqual verifiy if the return of the function AddTwoNumbers with the parameters.
		// a (a = 4 here)
		// and b (b = 3 here).
		// is equal with the number 7
	  expect(AddTwoNumbers(a,b)).toEqual(7);
  });
	it("substract Numbers", function() {
		expect(SubtractTwoNumbers(a,b)).toEqual(1);
	});
	it("Compare Numbers to be Greater Than", function() {
		// we verify if a > b or 4 > 3
		expect(a).toBeGreaterThan(b);
	});
	it("Compare Numbers to be Less Than", function() {
		// we verify if b < a or 3 < 4
		expect(b).toBeLessThan(a);
	});
	it("verify a number null", function() {
		// we verify than a value is null
		expect(c).toBeNull();
	});
	it("verify a number not null", function() {
		// we verify than a value is not null
		expect(a).not.toBeNull();
		expect(b).not.toBeNull();
	});
});

describe("Testing Hello world", function() {
  it("Hello world without params", function() {
	  expect(HelloWorld()).toEqual("Hello World");
  });
	it("Hello world function without params", function() {
		// we verify here if the function HelloWorld
		// who contains a part of string indicated
		expect(HelloWorld()).toContain("Hello");
	  expect(HelloWorld()).toContain("World");
  });
	it("The  Example of toContain() with an array",function (){
		array = [1, 2, 3, 4];
 		expect(array).toContain(3);
		expect(array[2].length).not.toBe(0);
	});
	it("Hello world function with params", function() {
		var result_expected = "Hello world i learn jasmine test.";
		// here we expect to have as result the value of variable result_expected
		expect(HelloWorld("i learn jasmine test.")).toEqual(result_expected);
	});
});

describe ("Testing the function is_defined", function () {
	it("is_defined tested when we have defined a param", function() {
		// we verify the return of the function is true
	 expect(is_defined(true)).toBe(true);
	});
	it("is_defined tested when we dont have param", function() {
		//We veifiy the condition of the function is here false
		expect(is_defined()).toBe(false);
	});
});

describe("Testing the function who can call a throw", function () {
	// As indicated for each "it" of this group we advise the global variable.
	// On the same princip we can use AfterEach.
	// This operation take the same but after each "it"
	beforeEach(function() {
		// global variable
		a = 1;
		b = 2;
		c = NaN;
	});

	// here we attach a example of AfterEach
	afterEach(function() {
	a, b = NaN;
	});

	it("Not Exception tested when we have the params", function() {
		// we test if the function return not a throw in using the function AddTwoNumbers
		// a and b are defined because we have the beforeEach
		var add_number_with_params = function() { AddTwoNumbers(a,b) };
		expect(add_number_with_params).not.toThrow();
	});
	it("Exception tested when we don't have all params", function() {
		// we test if the function return a throw in using the function AddTwoNumbers
		var add_number_without_all_params = function() { AddTwoNumbers(a,c) };
		expect(add_number_without_all_params).toThrow();
	});
	it("Exception tested when we don't have all params", function() {
		// we test if the function return not a throw in using the function AddTwoNumbers
		var add_number_without_params = function() { AddTwoNumbers(a,c) };
		// here if we compare with the precedent we can see the comment inside.
		expect(add_number_without_params).toThrow('Parameter is not a number or is not defined!');
	});
});

describe("Testing the function who can call a throw type Error", function () {
	beforeEach(function() {
		a = 1;
		b = 2;
		c = NaN;
	});

	it("Exception type error tested", function() {
		// we test if the function return a throw type error
		var type_error_exception = function() { is_exception() };
		expect(type_error_exception).toThrowError(TypeError);
		expect(type_error_exception).toThrowError(TypeError, "A type error throw");
	});
});

// xdescribe allow to disable the jasmine test.
xdescribe("Testing the function who can call a throw type Error", function () {
	beforeEach(function() {
		a = 1;
		b = 2;
		c = NaN;
	});

	it("Exception type error tested", function() {
		// we test if the function return a throw type error
		var type_error_exception = function() { is_exception() };
		expect(type_error_exception).toThrowError(TypeError);
		expect(type_error_exception).toThrowError(TypeError, "A type error throw");
	});
});

// on the up we have see how did we mark peding a group "describe"
// to know we can take the same for "it" but we have many ways to do.
describe("Pending specs", function() {
	// we can proceed on the same method and advise "xit"
	xit("can be declared 'xit'", function() {
	 expect(true).toBe(false);
 });
 // we can declare a "it" block without operations
 it("can be declared with 'it' but without a function");

 it("can be declared 'it' in pending", function() {
	expect(true).toBe(false);
	pending();
 });
});
