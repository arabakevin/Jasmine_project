function AddTwoNumbers(a,b) {
  if (isNaN(a) || isNaN(b)) {
    throw "Parameter is not a number or is not defined!";
  }else{
    return a + b; //Function to return addition of two numbers a and b.
  }

}

function SubtractTwoNumbers(a,b) {
	  return a - b; //Function to return subtract of two numbers a and b.
}

function HelloWorld(params){
  if (params === undefined) {
    return "Hello World";
  } else {
    return "Hello world " + params;
  }
}

function is_defined(params){
  if (params === undefined){
    return false;
  }else{
    return true;
  }
}

function is_exception(){
   throw new TypeError("A type error throw");
}
