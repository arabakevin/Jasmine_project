# Jasmine project

## My first steps in jasmine to start well


 In a first time you can dowload the last version of Jasmine in wip format  [HERE](https://github.com/jasmine/jasmine/releases)
 unzip the zip folder and move this inside your project.


 In a second time you create a file "functions.js"
 And copy/paste this JS code 

```javascript

function AddTwoNumbers(a,b) {
  return a + b; //Function to return addition of two numbers a and b.
}

function SubtractTwoNumbers(a,b) {
  return a - b; //Function to return subtract of two numbers a and b.
}

```

 With the picture attached you can see the result

![](assets/visualisationProject.png)

 In the third time we need create us first test in Jasmine
  Create a file in following this path "jasmine-standalone-3.2.0/spec/TestFunctions.js"
  And we can copy/paste your first test.

we can see a lot of code but you have a lot explanatory commentary.
 
```
// Jasmine Testing
// "describe" and "it" are present to oragnize properly your test.
// We can see as "describe" is the group and "it" an action of the group.

// Describe is similar at a general function
// where is included two parameters
// the first is a simple title of the "it" group.
// the second is function

describe("Testing Two Numbers", function() {
  var a = 4;
  var b = 3;
  // "it" is the same of describe
  it("Add Numbers", function() {
    // toEqual verifiy if the return of the function AddTwoNumbers with the parameters.
    // a (a = 4 here)
    // and b (b = 3 here).
    // is equal with the number 7
    expect(AddTwoNumbers(a,b)).toEqual(7);
  });
});

```

And in the last step you need change the "SpecRunner.html" in the folder Jasmine uzipped before.
## 
Verify than your file looks like that.

```html
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Jasmine Spec Runner v3.2.0</title>



  <link rel="shortcut icon" type="image/png" href="lib/jasmine-3.2.0/jasmine_favicon.png">
  <link rel="stylesheet" href="lib/jasmine-3.2.0/jasmine.css">

  <script src="lib/jasmine-3.2.0/jasmine.js"></script>
  <script src="lib/jasmine-3.2.0/jasmine-html.js"></script>
  <script src="lib/jasmine-3.2.0/boot.js"></script>

  <!-- include source files here... -->
  <script type="text/javascript" src="../functions.js"></script>

  <!-- include spec files here... -->
  <script type="text/javascript" src="spec/TestFunctions.js"></script>

</head>

<body>
</body>
</html>
```

__Running the Jasmine Test__

 Double click on "SpecRunner.html" and you can see the result directly on your browser.

 ![](assets/resultBrowser.png)


