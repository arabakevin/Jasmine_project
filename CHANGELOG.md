# Change Log

## Unreleased

## 1.02.00 (2018-09-11)
- Adding pending it and describe tests
- Adding throw type error tests
- Adding throw tests
- Example of beforeEach and AfterEach in the tests.
- Adding tests nil and with a string

## 1.01.00 (2018-09-08)
- Modification architecture and remove useless files


## 1.00.00 (2018-09-08)
 - Initialize the project witht the first jasmine test
